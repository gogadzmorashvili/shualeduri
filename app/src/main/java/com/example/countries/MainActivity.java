package com.example.countries;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toolbar;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar mToolBar;
    ListView mListView;

    String[] countryNames = {"Albania","Argentina","Belgium","Brazil","British","Chile","Colombia","Czech","Egypt","Finland","France","Georgia","Germany","Italy","Netherland","Norway","Portugal","Russia","Ukrain"};
    int[] countryFlags = {R.drawable.download,
            R.drawable.argentina,
            R.drawable.bel,
            R.drawable.brazil,
            R.drawable.british,
            R.drawable.cl,
            R.drawable.co,
            R.drawable.cz,
            R.drawable.eg,
            R.drawable.fin,
            R.drawable.fr,
            R.drawable.ge,
            R.drawable.ger,
            R.drawable.it,
            R.drawable.nl,
            R.drawable.nw,
            R.drawable.pt,
            R.drawable.ru,
            R.drawable.uk};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBar.setTitle(getResources().getString(R.string.app_name));
        mListView = (ListView) findViewById(R.id.listview);
        MainAdapter mainAdapter = new MainAdapter(MainActivity.this, countryNames, countryFlags);
        mListView.setAdapter(mainAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent mIntent = new Intent(MainActivity.this, DetailActivity.class);
                mIntent.putExtra("countryName", countryNames[i]);
                mIntent.putExtra("countryFlag", countryFlags[i]);
                startActivity(mIntent);

            }
        });
    }
}
